package jp.hyodo.tddbyexample;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class MoneyCalculatorTest {

    private MoneyCalculator instance;

    @Before
    public void setUp() {
        instance = new MoneyCalculator();
    }

    @Test
    public void testAddSameCurrency() {
        //Given
        final int expectedValue = 133;
        final String expectedCurrency = "BRL";

        final Money m1 = new Money("BRL", 100);
        final Money m2 = new Money("BRL", 33);

        //When
        final Money added = instance.add(m1, m2);

        //Then
        assertEquals(expectedValue, added.getValue());
        assertEquals(expectedCurrency, added.getCurrency());
    }

    @Test
    public void testAddSameCurrency2() {
        //Given
        final int expectedValue = 3004123;
        final String expectedCurrency = "JPY";

        final Money m1 = new Money("JPY", 3000000);
        final Money m2 = new Money("JPY", 4123);

        //When
        final Money added = instance.add(m1, m2);

        //Then
        assertEquals(expectedValue, added.getValue());
        assertEquals(expectedCurrency, added.getCurrency());
    }

    @Test
    public void testAddDifferentCurrencies() {
        /*
        * EUR 0.85 = USD 1
        */

        //Given
        final int expectedValue = 185;
        final String expectedCurrency = "EUR";

        final Money m1 = new Money("EUR", 100);
        final Money m2 = new Money("USD", 100);

        //When
        final Money added = instance.add(m1, m2);

        //Then
        assertEquals(expectedValue, added.getValue());
        assertEquals(expectedCurrency, added.getCurrency());
    }
}
