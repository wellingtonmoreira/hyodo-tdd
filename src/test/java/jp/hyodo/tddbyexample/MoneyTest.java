package jp.hyodo.tddbyexample;

import org.junit.Assert;
import org.junit.Test;

public class MoneyTest {

    @Test
    public void testMoney() {
        //Given - When
        final Money instance = new Money("BRL", 100);
        final Money instance2 = new Money("JPY", 5000);
        final Money instance3 = new Money("EUR", 350);
        final Money instance4 = new Money("USD", 5);

        //Then
        Assert.assertEquals("Value must be 100!", 100, instance.getValue());
        Assert.assertEquals("Currency must be BRL!", "BRL", instance.getCurrency());

        Assert.assertEquals("Value must be 5000!", 5000, instance2.getValue());
        Assert.assertEquals("Currency must be JPY!", "JPY", instance2.getCurrency());

        Assert.assertEquals("Value must be 350!", 350, instance3.getValue());
        Assert.assertEquals("Currency must be EUR!", "EUR", instance3.getCurrency());

        Assert.assertEquals("Value must be 5!", 5, instance4.getValue());
        Assert.assertEquals("Currency must be USD!", "USD", instance4.getCurrency());
    }

}
