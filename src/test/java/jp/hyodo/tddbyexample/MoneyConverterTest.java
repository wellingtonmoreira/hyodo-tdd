package jp.hyodo.tddbyexample;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MoneyConverterTest {

    private MoneyConverter instance;

    @Before
    public void setUp() {
        instance = new MoneyConverter();
    }

//    @Test
//    public void testConvertUSDtoBRL() {
//        //Given
//        final Money money = new Money("USD", 1);
//        final String expectedCurrency = "BRL";
//        final int expectedValue = 3;
//
//
//        //When
//        final Money returned = instance.convert(money, "BRL");
//
//        //Then
//        Assert.assertEquals(expectedCurrency, returned.getCurrency());
//        Assert.assertEquals(expectedValue, returned.getValue());
//    }

//    @Test
//    public void testConvertEURtoBRL() {
//        //Given
//        final Money money = new Money("EUR", 3);
//        final String expectedCurrency = "BRL";
//        final int expectedValue = 12;
//
//
//        //When
//        final Money returned = instance.convert(money, "BRL");
//
//        //Then
//        Assert.assertEquals(expectedCurrency, returned.getCurrency());
//        Assert.assertEquals(expectedValue, returned.getValue());
//    }

    @Test
    public void testConvertBRLtoUSD() {
        //Given
        final Money money = new Money("BRL", 3);
        final String expectedCurrency = "USD";
        final int expectedValue = 1;


        //When
        final Money returned = instance.convert(money, expectedCurrency);

        //Then
        Assert.assertEquals(expectedCurrency, returned.getCurrency());
        Assert.assertEquals(expectedValue, returned.getValue());
    }

    @Test
    public void testConvertEURtoUSD() {
        //Given
        final Money money = new Money("EUR", 4);
        final String expectedCurrency = "USD";
        final int expectedValue = 2;


        //When
        final Money returned = instance.convert(money, expectedCurrency);

        //Then
        Assert.assertEquals(expectedCurrency, returned.getCurrency());
        Assert.assertEquals(expectedValue, returned.getValue());
    }

    @Test
    public void testGetRate() {
        //Given
        final String currency = "BRL";
        final int expectedRateValue = 3;

        //When
        final Rate rate = instance.getRate(currency);

        //Then
        Assert.assertEquals(currency, rate.getCurrency());
        Assert.assertEquals(expectedRateValue, rate.getValue());
    }

    @Test
    public void testGetRate2() {
        //Given
        final String currency = "ARG";
        final int expectedRateValue = 30;

        //When
        final Rate rate = instance.getRate(currency);

        //Then
        Assert.assertEquals(currency, rate.getCurrency());
        Assert.assertEquals(expectedRateValue, rate.getValue());
    }

    @Test
    public void testGetRateNull() {
        //Given - When
        final Rate rate = instance.getRate(null);

        //Then
        Assert.assertEquals(Rate.UNDEFINED, rate);
    }

    @Test
    public void testGetRateNonExistent() {
        //Given - When
        final Rate rate = instance.getRate("XABLAU");

        //Then
        Assert.assertEquals(Rate.UNDEFINED, rate);
    }
}
