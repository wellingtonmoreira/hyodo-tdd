package jp.hyodo.tddbyexample;

public class Money {
    private final int value;
    private final String currency;

    public Money(final String currency, final int value) {
        this.value = value;
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public int getValue() {
        return value;
    }

}
