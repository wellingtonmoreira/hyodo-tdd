package jp.hyodo.tddbyexample;

public class MoneyCalculator {

    MoneyCalculator() {

    }

    public Money add(final Money augend, final Money addend) {
        final int value = augend.getValue() + addend.getValue();
        final String currency = augend.getCurrency();
        return new Money(currency, value);
    }
}
