package jp.hyodo.tddbyexample;

public class MoneyConverter {

    public Money convert(final Money money, final String currency) {
        final Rate rateFrom = getRate(money.getCurrency());
        final Rate rateTo = getRate(currency);
        final float conversionRate = ((float) rateTo.getValue()) / ((float) rateFrom.getValue());
        final float result = money.getValue() * conversionRate;

        return new Money(currency, (int) result);
    }

    Rate getRate(final String rate) {
        try {
            return Rate.valueOf(rate);
        } catch (final Exception exc) {
            return Rate.UNDEFINED;
        }
    }

}

enum Rate {
    BRL("BRL", 3), USD("USD", 1), ARG("ARG", 30), EUR("EUR", 2), UNDEFINED("UNDEFINED", -1);

    private final String currency;
    private final int rate;

    private Rate(final String currency, final int rate) {
        this.currency = currency;
        this.rate = rate;
    }

    String getCurrency() {
        return currency;
    }

    int getValue() {
        return rate;
    }
}
